package device

type Powerswitch interface {
	Probe() error // check if device is of the right kind
	PowerOn(outlet int) error
	PowerOff(outlet int) error
	Status() (map[int]bool, error)
}

func PowerOn(powerswitch Powerswitch, outlet int) error {

	return powerswitch.PowerOn(outlet)

}

func PowerOff(powerswitch Powerswitch, outlet int) error {

	return powerswitch.PowerOff(outlet)

}

func Status(powerswitch Powerswitch) (map[int]bool, error) {

	return powerswitch.Status()

}
