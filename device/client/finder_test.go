package client_test

import (
	"bitbucket.org/orangeparis/pdu/device"
	"bitbucket.org/orangeparis/pdu/device/client"
	"testing"
)

func TestGuessPowerswtichDevice(t *testing.T) {

	parameters := &device.DeviceParameters{IP: "127.0.0.1", Password: "myPassword"}

	h, err := client.GuessPowerswtichDevice(parameters)
	_ = h
	if err != nil {
		t.Errorf("finder error: %s\n", err.Error())
		return
	}

	outlet := 1
	err = h.PowerOn(outlet)
	err = h.PowerOff(outlet)
	status, err := h.Status()
	_ = status

}
