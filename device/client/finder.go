package client

import (
	"bitbucket.org/orangeparis/pdu/device"
	"bitbucket.org/orangeparis/pdu/device/energenie"
	"bitbucket.org/orangeparis/pdu/device/ippower"
	"errors"
	"log"
)

/*

	try to guess the type of powerswitch device ( ippower or energenie )

	usage:

		see finder_test.go


*/

func GuessPowerswtichDevice(param *device.DeviceParameters) (handler device.Powerswitch, err error) {

	// try ippower

	ip := param.IP

	// create ippower default parameters
	defaultParameters := ippower.NewDeviceParameters(ip)
	// updated with given parameters
	parameters := UpdatedParameters(defaultParameters, param)
	// build a client with it
	x, err := ippower.NewClient(ip, *parameters)

	// probe check if device is of the right kind
	err = x.Probe()
	if err == nil {
		// ok it is an ippower return the handler
		return x, err
	}

	// we got to try another one
	// create ippower default parameters
	defaultParameters = energenie.NewDeviceParameters(ip)
	// updated with given parameters
	parameters = UpdatedParameters(defaultParameters, param)
	// build a client with it
	y, err := energenie.NewClient(ip, *parameters)

	err = y.Probe()
	if err == nil {
		// ok it is an energenie device return the handler
		return x, err
	}

	err = errors.New("Cannot find a powerswitch")
	return handler, err

}

func UpdatedParameters(source, update *device.DeviceParameters) *device.DeviceParameters {

	n := &device.DeviceParameters{source.IP, source.Port, source.User, source.Password}

	if update.IP != "" {
		n.IP = update.IP
	}
	if update.Port != 0 {
		n.Port = update.Port
	}
	if update.User != "" {
		n.User = update.User
	}
	if update.Password != "" {
		n.Password = update.Password
	}
	return n
}

func usage(ip string, port int) {

	p := &device.DeviceParameters{
		IP:       ip,
		Port:     port,
		User:     "",
		Password: "",
	}

	handler, err := GuessPowerswtichDevice(p)
	if err != nil {
		log.Printf("cannot guess pdu type:%s\n", err.Error())
	}
	outlet := 1
	err = handler.PowerOn(outlet)
	err = handler.PowerOff(outlet)
	status, err := handler.Status()
	_ = status

}
