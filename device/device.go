package device

type DeviceParameters struct {
	IP       string
	Port     int
	User     string
	Password string
}
