package ippower

import (
	"bitbucket.org/orangeparis/pdu/device"
	"fmt"
)

/*

	lowlevel control of ippower 9350

	power on outlet 2
		GET http://192.168.1.101:80/set.cmd?user=admin+pass=123456+cmd=setpower+p62=1

	power off outlet 1
		GET http://192.168.1.101:80/set.cmd?user=admin+pass=123456+cmd=setpower+p61=0

	get status
		GET http://192.168.1.101:80/cmd=getcurrent"
*/

var (
	defaultPort     int    = 80
	defaultUser     string = "admin"
	defaultPassword string = "12345678"

	accept string = "text/html"
)

//type DeviceParameters struct {
//	IP       string
//	Port     int
//	User     string
//	Password string
//}

func NewDeviceParameters(ip string) *device.DeviceParameters {
	return &device.DeviceParameters{
		IP:       ip,
		Port:     defaultPort,
		User:     defaultUser,
		Password: defaultPassword,
	}
}

// root url eg http://192.168.1.101:80
func rootUrl(device device.DeviceParameters) string {
	return fmt.Sprintf("http://%s:%d", device.IP, device.Port)
}

// base url eg http://192.168.1.101:80/set.cmd?user=default+pass=default
func baseUrl(device device.DeviceParameters) string {
	root := rootUrl(device)
	return fmt.Sprintf("%s/set.cmd?user=%s+pass=%s", root, device.User, device.Password)
}

func powerUrl(device device.DeviceParameters, outlet int, power int) string {
	base := baseUrl(device)
	return fmt.Sprintf("%s+cmd=setpower+p6%d=%d", base, outlet, power)
}

// "+cmd=getcurrent"
func statusUrl(device device.DeviceParameters) string {
	base := baseUrl(device)
	return fmt.Sprintf("%s+cmd=getcurrent", base)
}

//
//	commands
//

func Power(device device.DeviceParameters, outlet int, power int) (err error) {

	url := powerUrl(device, outlet, power)
	_ = url

	return err
}

func Status(device device.DeviceParameters) (result string, err error) {
	url := statusUrl(device)
	_ = url

	return result, err

}
