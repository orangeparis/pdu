package ippower

import (
	//"bitbucket.org/orangeparis/pdu/device/ippower"
	"testing"
)

func TestNewDeviceParameters(t *testing.T) {

	d := NewDeviceParameters("127.0.0.1")

	if rootUrl(*d) != "http://127.0.0.1:80" {
		t.Fail()
	}

	if baseUrl(*d) != "http://127.0.0.1:80/set.cmd?user=admin+pass=12345678" {
		t.Fail()
	}

	// switch ON outlet 2
	if powerUrl(*d, 2, 1) != "http://127.0.0.1:80/set.cmd?user=admin+pass=12345678+cmd=setpower+p62=1" {
		t.Fail()
	}

	// switch OFF outlet 1
	if powerUrl(*d, 1, 0) != "http://127.0.0.1:80/set.cmd?user=admin+pass=12345678+cmd=setpower+p61=0" {
		t.Fail()
	}

	//
	if statusUrl(*d) != "http://127.0.0.1:80/set.cmd?user=admin+pass=12345678+cmd=getcurrent" {
		t.Fail()
	}

}
