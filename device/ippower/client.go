package ippower

import (
	"bitbucket.org/orangeparis/pdu/device"
	"errors"
	//"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

// Client: structure of a IpPower client
type Client struct {
	// structure of a IpPower client

	http.Client

	Device device.DeviceParameters

	Addr string
	//Passwd string
	//opened bool
	//logged bool

}

func NewClient(ip string, device device.DeviceParameters) (*Client, error) {
	// return a Client if we can reach it , return err if not reachable
	//timeout := 1 * time.Second

	timeout := 2 * time.Second
	var err error = nil

	netAddr, err := NetAddr(ip)
	if err != nil {
		return nil, err
	}

	client := http.Client{
		Timeout: timeout,
	}

	// connected
	e := &Client{client, device, netAddr.String()}

	return e, err

}

func (e *Client) Probe() error {
	// try to recognise an ippower device
	url := fmt.Sprintf("http://%s", e.Addr)
	r, err := e.Get(url)
	//r,err:= e.Head(url)
	if err != nil {
		// cannot reach device
		fmt.Println("IpPower.Client.Probe: ", err)
		return err
	}

	_ = r
	// we got a http response
	//fmt.Printf("response (%d) : %s\n",r.StatusCode, r)

	// analyse header to find header server=EG-Web
	//if header, ok := r.Header["Server"]; ok {
	//	server := string(header[0])
	//	if server == EnergenieServerHeader {
	//		// YES we got a http HEADER Server equal to EG-Web
	//		//fmt.Printf("Header Server: %v\n", server)
	//		return nil
	//	}
	//}
	// not an energenie web server
	//err = errors.New("not an energenie device ")
	return err
}

//
// implements Powerswitch interface
//
func (e *Client) PowerOn(outlet int) error {
	// switch on outlet [1-4]
	if outlet < 1 || outlet > 4 {
		err := errors.New("Invalid outlet (must be in [1,4]")
		return err
	}

	url := powerUrl(e.Device, outlet, 1)
	_, err := e.Get(url) //   "text/plain
	return err
}

func (e *Client) PowerOff(outlet int) error {
	// switch off outlet [1-4]
	// url = self.base_url + "?cte%d=1" % int(channel)

	if outlet < 1 || outlet > 4 {
		err := errors.New("Invalid outlet (must be in [1,4])")
		return err
	}

	url := powerUrl(e.Device, outlet, 0)
	_, err := e.Get(url) //   "text/plain
	return err
}

func (e *Client) Status() (map[int]bool, error) {
	// try to log in , a POST host/login.html   pw=<password>

	var err error = nil
	response := make(map[int]bool)

	url := statusUrl(e.Device)
	r, err := e.Get(url)
	if err != nil {
		return response, err
	}
	buf, err := ioutil.ReadAll(r.Body)
	if err == nil {
		// ok
		response, err = ExtractStatusInfo(string(buf))
	}
	return response, err
}

// utils

func ExtractStatusInfo(text string) (map[int]bool, error) {
	// extract status info from getconfig command

	var err error = nil
	result := make(map[int]bool)
	//
	//// search for  var sockstates = [1,1,1,1];
	//found := status_regexp.FindString(text)
	//if len(found) > 0 {
	//	//fmt.Println(found)
	//
	//	array := status_regexp.FindStringSubmatch(text)
	//	// update status
	//	for i := 0; i < 4; i++ {
	//		if array[i+1] == "1" {
	//			result[i] = true
	//		} else {
	//			result[i] = false
	//		}
	//	}
	//
	//} else {
	//	msg := "Energenie cannot find status string in page"
	//	fmt.Println(msg)
	//	err = errors.New(msg)
	//}

	return result, err
}

func NetAddr(ip string) (net.Addr, error) {

	if strings.Contains(ip, ":") {
		// full ip
	} else {
		// add default port
		ip = fmt.Sprintf("%s:%d", ip, defaultPort)
	}
	tcpAddr, err := net.ResolveTCPAddr("tcp4", ip)

	return tcpAddr, err
}
