package service

import (
	"bitbucket.org/orangeparis/pdu/device"
	"bitbucket.org/orangeparis/pdu/device/client"
	"log"
	"time"

	"github.com/jirenius/go-res"
)

/*


	livebox.dora.wait { fti , timeout }  -> { doraEvent }

*/

// WaitIn : params for call.livebox.dora.wait
type PowerIn struct {
	IP     string `json:"ip"`     // pdu ip address
	Outlet int    `json:"outlet"` // pdu outlet
	//Port   int 	`json:"port"`
	Timeout uint64 `json:"timeout"` // timeout in seconds
}

type StatusOut struct {
	Status map[int]bool `json:"status"` //
}

// pdu.name :  handle resource livebox.dora
func AddPduResources(s *res.Service, name string) {

	s.Handle(name,
		res.Access(res.AccessGranted),

		// respond to pdu.<name>.ping
		res.Call("ping", func(r res.CallRequest) {
			r.OK("PONG")
		}),

		// handle call.pdu.<name>.on
		res.Call("on", func(r res.CallRequest) {
			powerOnOff(r, true)
			return
		}),

		// handle call.pdu.<name>.off
		res.Call("on", func(r res.CallRequest) {
			powerOnOff(r, false)
			return
		}),

		// handle call.pdu.<name>.status
		res.Call("status", func(r res.CallRequest) {
			var p = PowerIn{}
			r.ParseParams(&p)

			// create a pdu controller
			h, err := findPdu(r, p.IP)
			if err != nil {
				return
			}

			//parameters := &device.DeviceParameters{IP: p.IP }
			//h, err := client.GuessPowerswtichDevice(parameters)
			//_ = h
			//if err != nil {
			//	log.Printf("pdu finder error: %s\n", err.Error())
			//	rerr := &res.Error{Code: "500", Message: err.Error()}
			//	r.Error(rerr)
			//	return
			//}

			// call status
			status, err := h.Status()
			if err != nil {
				log.Printf("pdu status error: %s\n", err.Error())
				rerr := &res.Error{Code: "500", Message: err.Error()}
				r.Error(rerr)
				return
			}
			r.OK(status)

		}),
	)

}

func findPdu(r res.CallRequest, ip string) (handler device.Powerswitch, err error) {

	// create a pdu controller
	parameters := &device.DeviceParameters{IP: ip}
	h, err := client.GuessPowerswtichDevice(parameters)
	_ = h
	if err != nil {
		log.Printf("pdu finder error: %s\n", err.Error())
		rerr := &res.Error{Code: "500", Message: err.Error()}
		r.Error(rerr)
		return
	}
	return handler, nil

}

func powerOnOff(r res.CallRequest, on bool) {

	var p = PowerIn{}
	r.ParseParams(&p)

	// set timeout in seconds
	//timeout := time.Duration(p.Timeout) * time.Second

	// extend service timeout to match requested timeout
	serviceTimeout := time.Duration(p.Timeout+2) * time.Second

	// extend service timeout to match requested timeout
	r.Timeout(serviceTimeout)

	// create a pdu controller
	parameters := &device.DeviceParameters{IP: p.IP}

	h, err := client.GuessPowerswtichDevice(parameters)
	_ = h
	if err != nil {
		log.Printf("pdu finder error: %s\n", err.Error())
		rerr := &res.Error{Code: "500", Message: err.Error()}
		r.Error(rerr)
		return
	}
	outlet := p.Outlet
	if on == true {
		err = h.PowerOn(outlet)
	} else {
		err = h.PowerOff(outlet)
	}
	if err != nil {
		rerr := &res.Error{Code: "500", Message: err.Error()}
		r.Error(rerr)
		return
	}
	r.OK(true)

}
