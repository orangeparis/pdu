package main

import (
	"context"
	"flag"
	//"fmt"
	"log"
	"sync"

	"bitbucket.org/orangeparis/ines/heartbeat"
	"bitbucket.org/orangeparis/pdu/service"

	"github.com/jirenius/go-res"
)

/*
	runs a pdu nats server

	runs nats services

		call.pdu.0.on
			ip : string
			port: int

*/

func main() {

	var natsUrl string
	var name string

	flag.StringVar(&natsUrl, "nats", "nats://127.0.0.1:4222", "nats server url")
	flag.StringVar(&name, "name", "0", "pdu handler name ( eg: labo ) ")

	flag.Parse()

	// s
	log.Printf("pdu : starts with nats server at : %s\n", natsUrl)

	// context
	//ctx, cancel := context.WithCancel(context.Background())
	//defer cancel()

	var wg sync.WaitGroup

	// create the lbservices
	s := res.NewService("pdu")

	// add pdu resources
	service.AddPduResources(s, name)

	// Start the service
	go s.ListenAndServe(natsUrl)

	// start the heartbeat emiter
	e, _ := heartbeat.NewEmitter("", natsUrl, 5)
	e.Start()

	// wait here
	wg.Wait()

	log.Printf("pdu: exit")
}
